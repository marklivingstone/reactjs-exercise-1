import React, { Component } from "react";
import {
  Modal,
  InputGroup,
  FormControl,
  Button,
  ToggleButtonGroup,
  ToggleButton,
} from "react-bootstrap";

import "./names.styles.scss";

class Names extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: true,
      player1: "Player 1",
      player2: "Player 2",
      p1As: "",
      p2As: "",
    };
  }

  handleClose = () => {
    console.log("Can't close");
  };

  handleChange = (e) => {
    const inputId = e.target.id;
    this.setState({ [inputId]: e.target.value });
  };

  handleSelection = (e) => {
    e.preventDefault();

    if (e.target.value === "X") {
      this.setState({ p1As: "X" });
      this.setState({ p2As: "O" });
    } else {
      this.setState({ p1As: "O" });
      this.setState({ p2As: "X" });
    }
  };

  handleClick = (e) => {
    e.preventDefault();

    if (this.state.p1As !== "") {
      this.props.setNames(
        this.state.player1,
        this.state.player2,
        this.state.p1As,
        this.state.p2As
      );
    }
  };

  render() {
    return (
      <Modal
        className="name-modal"
        show={this.state.show}
        onHide={this.handleClose}
      >
        <Modal.Header closeButton>
          <Modal.Title className="text-center w-100">Tic Tac Toe</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text>Player 1</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                aria-label="Player 1"
                aria-describedby="player1"
                id="player1"
                onChange={this.handleChange}
                placeholder={this.state.player1}
              />
            </InputGroup>
            <InputGroup className="mb-3">
              <InputGroup.Prepend>
                <InputGroup.Text>Player 2</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                aria-label="Player 2"
                aria-describedby="player2"
                id="player2"
                onChange={this.handleChange}
                placeholder={this.state.player2}
              />
            </InputGroup>
          </div>
          <div className="player-select">
            <h2>Select a letter to begin with (Player 1)</h2>
            <ToggleButtonGroup type="radio" name="options" className="mb-2">
              <ToggleButton value="X" onChange={this.handleSelection}>
                X
              </ToggleButton>
              <ToggleButton value="O" onChange={this.handleSelection}>
                O
              </ToggleButton>
            </ToggleButtonGroup>
          </div>
          <div className="w-100 text-center">
            <Button
              className="btn name-modal-btn"
              variant="light"
              onClick={this.handleClick}
            >
              Submit
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

export default Names;
