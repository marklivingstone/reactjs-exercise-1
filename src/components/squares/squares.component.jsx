import React from "react";

import "./squares.styles.scss";

const Squares = ({ onClick, value }) => {
  return (
    <button className="square" onClick={onClick}>
      {value}
    </button>
  );
};

export default Squares;
