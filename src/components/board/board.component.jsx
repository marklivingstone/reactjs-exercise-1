import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";

import "./board.styles.scss";

// Components
import Squares from "../squares/squares.component";
import GameHistory from "../game-history/game-history.component";

import { winningPatterns } from "./winningPatterns";

const Board = ({ player1, player2 }) => {
  const [board, setBoard] = useState(Array(9).fill(null));
  const [player, setPlayer] = useState([]);
  const [gameHistory, setGameHistory] = useState([]);
  useEffect(() => {
    setPlayer(player1);
  }, []);

  useEffect(() => {
    handlePlayerChange();

    checkForTie();
  }, [board]);

  const handleClick = (square) => {
    if (board[square] === null) {
      setBoard(
        board.map((val, idx) => {
          if (idx === square) {
            return player.as;
          }
          return val;
        })
      );

      if (player === player1) {
        player1.moves += 1;
      }

      if (player === player2) {
        player2.moves += 1;
      }
    }
  };

  const handlePlayerChange = () => {
    if (player.name === player1.name && player.as === player1.as) {
      setPlayer(player2);
    } else {
      setPlayer(player1);
    }
  };

  const displaySquare = (idx) => {
    return <Squares value={board[idx]} onClick={() => handleClick(idx)} />;
  };

  const checkWinner = () => {
    let hasWinner = false;
    winningPatterns.forEach((currentPattern) => {
      const thePlayer = board[currentPattern[0]];

      if (thePlayer === null) return;

      let isFound = true;
      currentPattern.forEach((idx) => {
        if (board[idx] !== thePlayer) {
          isFound = false;
        }
      });
      if (isFound) {
        alert(`${player.name} Wins!`);
        setGameHistory([
          ...gameHistory,
          { name: `${player.name} wins`, moves: player.moves, as: player.as },
        ]);

        if (player === player1) {
          player1.score += 1;
        }

        if (player === player2) {
          player2.score += 1;
        }

        resetBoard();
        hasWinner = true;
      }
    });
    return hasWinner;
  };

  const checkForTie = () => {
    if (!checkWinner()) {
      let isFilled = true;
      board.forEach((square) => {
        if (square === null) {
          isFilled = false;
        }
      });

      if (isFilled) {
        alert("Draw!");
        setGameHistory([
          ...gameHistory,
          { name: "Draw", moves: "N/A", as: "" },
        ]);
        resetBoard();
      }
    }
  };

  const resetBoard = () => {
    setBoard(Array(9).fill(null));
    player1.moves = 0;
    player2.moves = 0;
  };

  return (
    <div className="game">
      <Container>
        <Row>
          <Col md={6} className="text-center">
            <div className="player-details">
              {player.name}'s Turn ({player.as})
            </div>
          </Col>
          <Col md={6} className="text-center">
            <div className="player-details">
              {player1.name}: {player1.score}
            </div>
            <div className="player-details">
              {player2.name}: {player2.score}
            </div>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col md={6} sm={12}>
            <div className="game-board">
              <div className="game-board_row">
                {displaySquare(0)}
                {displaySquare(1)}
                {displaySquare(2)}
              </div>
              <div className="game-board_row">
                {displaySquare(3)}
                {displaySquare(4)}
                {displaySquare(5)}
              </div>
              <div className="game-board_row">
                {displaySquare(6)}
                {displaySquare(7)}
                {displaySquare(8)}
              </div>
            </div>
          </Col>
          <Col md={6} sm={12}>
            <div className="game-board_history">
              <GameHistory gameHistory={gameHistory} />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Board;
