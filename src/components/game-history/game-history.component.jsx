import React from "react";

import "./game-history.styles.scss";

// Components
import Rounds from "../rounds/rounds.component";

const GameHistory = ({ gameHistory }) => {
  return (
    <div>
      <h1>Game history</h1>
      <div className="game-history_container">
        {gameHistory.map((game, idx) => (
          <Rounds game={game} id={idx} key={idx} />
        ))}
      </div>
    </div>
  );
};

export default GameHistory;
