import React from "react";

import "./rounds.styles.scss";

const Rounds = ({ game, id }) => {
  return (
    <div className="round-result">
      Game {id + 1} - {game.name} - {game.moves} moves ({game.as})
    </div>
  );
};

export default Rounds;
