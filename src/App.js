import React, { useState } from "react";

import "./App.scss";

// Components
import Names from "./components/names/names.component";
import Board from "./components/board/board.component";

const App = () => {
  const [player1, setPlayer1] = useState([]);
  const [player2, setPlayer2] = useState([]);
  const [showPlayerModal, setShowPlayerModal] = useState(true);

  const nameHandler = (player1, player2, p1, p2) => {
    setPlayer1({ name: player1, as: p1, moves: 0, score: 0 });
    setPlayer2({ name: player2, as: p2, moves: 0, score: 0 });
    setShowPlayerModal(false);
  };
  return (
    <div className="App">
      {showPlayerModal ? (
        <Names setNames={nameHandler} />
      ) : (
        <Board player1={player1} player2={player2} />
      )}
    </div>
  );
};

export default App;
